#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int min = 1000;
    int max = 2000;
    int small;
    int med;
    int large;
    char addr [40];
    int zip;
    int id;
    int price;
    int bed;
    int bath;
    int area;
    FILE *f, *g, *l, *m, *s;//pointer delcaration for 2 reads and 3 writes
    f = fopen("homes200.csv", "r");
    g = fopen("homelistings.csv", "r");
    s = fopen("small.csv", "w");
    m = fopen("med.csv", "w");
    l = fopen("large.csv", "w");
    
    if (f == NULL || g == NULL)
    {
        printf("Couldn't open file for reading\n");
        exit(1);
    }
    
    if (l == NULL || s == NULL || m == NULL)
    {
        printf("Couldn't open file to write to\n");
        exit(1);
    }
    
    while (fscanf(f, "%d,%d,%[^,],%d,%d,%d,%d", &zip, &id, addr, &price, &bed, &bath, &area) != EOF)
    // switch this^ value for other file XD
    {
        if(area < min)
        {
            fprintf(s, "%s : %d\n", addr, area);//reads values from scanf into file pointed at by s
        }
        
        if(area >= min && area <= max)
        {
            fprintf(m, "%s : %d\n", addr, area);//reads values from scanf into file pointed at by m
        }

        if(area > max)
        {
            fprintf(l, "%s : %d\n", addr, area);//reads values from scanf into file pointed at by l
        }
    }
    
    fclose(s);
    fclose(m);
    fclose(l);
   
}